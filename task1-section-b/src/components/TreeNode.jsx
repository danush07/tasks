import React from "react";
import Folder from "./Folder";
import File from "./File";

const TreeNode = ({ node }) => {
  if (node && node.map) {
    return node.map((item, index) => <TreeNode key={index} node={item} />);
  } else if (typeof node === "object" && node !== null) {
    const [folderName, folderContent] = Object.entries(node)[0];
    return (
      <Folder key={folderName} name={folderName} content={folderContent} />
    );
  } else {
    return <File key={node} name={node} />;
  }
};

export default TreeNode;
