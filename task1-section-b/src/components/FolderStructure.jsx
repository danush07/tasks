import React from "react";
import TreeNode from "./TreeNode";

const FolderStructure = ({ data }) => (
  <div>
    {data.map((item, index) => (
      <TreeNode key={index} node={item} />
    ))}
  </div>
);

export default FolderStructure;
