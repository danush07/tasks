import React, { useState } from "react";
import TreeNode from "./TreeNode";

const Folder = ({ name, content }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleFolder = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div>
      <div style={{ marginLeft: "10px" }} onClick={toggleFolder}>
        {isOpen ? "📂" : "📁"} {name}
      </div>
      {isOpen && (
        <div style={{ marginLeft: "20px" }}>
          {content.map((item, index) => (
            <TreeNode key={index} node={item} />
          ))}
        </div>
      )}
    </div>
  );
};

export default Folder;
