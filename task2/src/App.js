import React, { useState, useEffect } from "react";
import generateRandomData from "./utils/generateRandomData";

const LazyLoadingComponent = () => {
  const [data, setData] = useState(generateRandomData(100));
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (
        window.innerHeight + document.documentElement.scrollTop >=
        document.documentElement.offsetHeight - 10
      ) {
        loadMoreData();
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [data]);

  const loadMoreData = () => {
    if (!isLoading) {
      setIsLoading(true);
      setTimeout(() => {
        const newData = generateRandomData(5);
        setData((prevData) => [...prevData, ...newData]);
        setIsLoading(false);
      }, 1000);
    }
  };

  return (
    <div>
      {data.map((item, index) => (
        <div key={index}>{item}</div>
      ))}
      {isLoading && <div className="spinner"></div>}
      <span className="text">
      Hang on Content Loading...
      
      </span>
    </div>
  );
};

export default LazyLoadingComponent;
