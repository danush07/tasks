const generateRandomData = (count) => {
  const data = [];
  for (let i = 0; i < count; i++) {
    data.push(`Item ${Math.floor(Math.random() * 100)}`);
  }
  return data;
};

export default generateRandomData;
