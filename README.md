Section C

QNs 1: 
Write and share a small note about your choice of system to schedule periodic tasks (such as downloading list of ISINs every 24 hours). Why did you choose it? Is it reliable enough; Or will it scale? If not, what are the problems with it? And, what else would you recommend to fix this problem at scale in production?

1. ) For Sceduling periodic Tasks I would Suggest use of AWS, AWS CloudWatch Events and AWS Lambda My Previous Company Used these Technologies to Maintain Data and Users Credentials and thier Data Since it is Secure and we can create a CloudWatch Events rule to trigger a Lambda function at regular intervals. Lambda can then execute our task, such as downloading the list of ISINs. This setup is really  reliable  and can handle a variety of workflows


QNs 2:
Suppose you are building a financial planning tool - which requires us to fetch bank statements from the user. Now, we would like to encrypt the data - so that nobody - not even the developer can view it. What would you do to solve this problem?

2.) here we can implement end-to-end Encryption like whatsapp securing our chats 
and also have a seperate Data Base for Storage service like Amazon S3 with server-side encryption
